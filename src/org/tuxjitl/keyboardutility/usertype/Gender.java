package org.tuxjitl.keyboardutility.usertype;

/**
 * An enumeration of genders
 *
 * @author JCL
 * @version 1.0
 */
public enum Gender {
    FEMALE,
    MALE,
    OTHER;
}
