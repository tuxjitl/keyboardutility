package org.tuxjitl.keyboardutility.exceptions;

/**
 * Class for handling exceptions for Email address input format validation
 */
public class IllegalEmailException extends Throwable {

    public IllegalEmailException(String s) {

        super(s);

    }
}
