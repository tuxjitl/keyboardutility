package org.tuxjitl.keyboardutility.keyboardinput;

import org.tuxjitl.keyboardutility.exceptions.*;
import org.tuxjitl.keyboardutility.usertype.Gender;

import java.math.BigDecimal;
import java.time.*;
import java.util.InputMismatchException;
import java.util.Locale;
import java.util.Scanner;

public abstract class KeyboardUtility {

    //region Member variables

    private static final String INVALID_MSG = "Invalid input! Please try again...";
    public static final Scanner KEYBOARD = new Scanner(System.in).useLocale(Locale.ENGLISH);

    //endregion

    //region ask for text or character

    /**
     * Static method  for asking a string as input e.g. a name.
     *
     * @param message Show the prompt passed as argument: e.g. Enter your name.
     *
     * @return String Returns the input from the user: e.g. Malcom
     */
    public static String ask(String message) {

        while (true) {
            System.out.print(message + ": ");
            String input = KEYBOARD.nextLine().trim();
            try {
                if (input.length() > 0) {

                    return input;
                }
                else {
                    throw new InputMismatchException(INVALID_MSG);
                }
            }
            catch (InputMismatchException ime) {
                System.out.println(ime.getMessage());
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method  for asking a character as input e.g. a p.
     *
     * @param message Show the prompt passed as argument: e.g. Enter a character.
     *
     * @return Char Returns the input from the user: e.g. p.
     */
    public static char askForCharacter(String message) {

        while (true) {
            System.out.print(message + ": ");
            String input = KEYBOARD.nextLine().trim();
            try {
                if (input.length() > 0) {
                    char character = input.charAt(0);
                    return character;
                }
                else {
                    throw new InputMismatchException(INVALID_MSG);
                }
            }
            catch (InputMismatchException ime) {
                System.out.println(ime.getMessage());
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    //endregion

    //region ask for numbers

    /**
     * Static method  for asking a byte input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter the number between -128 - 127 of persons.
     *
     * @return byte Returns the input from the user: e.g. 5.
     */
    public static byte askForByte(String message) {

        while (true) {
            String input = ask(message);
            try {
                return Byte.parseByte(input);
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method  for asking a short input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter a short number.
     *
     * @return short Returns the input from the user: e.g. 5.
     */
    public static short askForShort(String message) {

        while (true) {
            String input = ask(message);
            try {
                return Short.parseShort(input);
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method  for asking an int input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter the number of persons.
     *
     * @return int Returns the input from the user: e.g. 5.
     */
    public static int askForInt(String message) {

        while (true) {
            String input = ask(message);
            try {
                return Integer.parseInt(input);
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method  for asking a long input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter a long number.
     *
     * @return long Returns the input from the user: e.g. 123654789555.
     */
    public static long askForLong(String message) {

        while (true) {
            String input = ask(message);
            try {
                return Long.parseLong(input);
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method for asking a float input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter the height.
     *
     * @return float Returns the input from the user: e.g. 2.54.
     */
    public static float askForFloat(String message) {

        while (true) {
            String input = ask(message);
            try {
                Float d = Float.parseFloat(input);
                if (!d.isInfinite()) {

                    return d;
                }
                else {
                    throw new InputMismatchException(INVALID_MSG);
                }
            }
            catch (InputMismatchException ime) {
                System.out.println(ime.getMessage());
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method for asking a double input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter the height.
     *
     * @return double Returns the input from the user: e.g. 2.54.
     */
    public static double askForDouble(String message) {

        while (true) {
            String input = ask(message);
            try {
                Double d = Double.parseDouble(input);
                if (!d.isInfinite()) {

                    return d;
                }
                else {
                    throw new InputMismatchException(INVALID_MSG);
                }
            }
            catch (InputMismatchException ime) {
                System.out.println(ime.getMessage());
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    /**
     * Static method  for asking a BigDecimal for currency purposes.
     *
     * @param message Show the prompt passed as argument: e.g. Enter the price.
     *
     * @return BigDecimal Returns the input from the user: e.g. 13.99.
     */
    public static BigDecimal askForBigDecimal(String message) {

        BigDecimal amount;
        while (true) {
            String input = ask(message);
            try {
                amount = new BigDecimal(input);

                if (!amount.equals(null)) {

                    return amount;
                }
                else {
                    throw new InputMismatchException(INVALID_MSG);
                }
            }
            catch (InputMismatchException ime) {
                System.out.println(ime.getMessage());
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    //endregion

    //region Ask for boolean

    /**
     * Static method for asking a boolean input.
     *
     * @param message Show the prompt passed as argument: e.g. Enter true/false.
     *
     * @return boolean Returns the input from the user: e.g. true.
     */
    public static boolean askForBoolean(String message) {

        while (true) {
            String input = ask(message).toLowerCase().trim();
            try {
                if (input.length() > 0) {
                    Boolean d = Boolean.parseBoolean(input.trim());

                    if (input == "true" || input == "false") {

                        return d;
                    }
                    else {
                        throw new InputMismatchException(INVALID_MSG);
                    }
                }
                else {
                    throw new InputMismatchException(INVALID_MSG);
                }
            }
            catch (InputMismatchException ime) {
                System.out.println(ime.getMessage());
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
            }
        }
    }

    //endregion

    //region ask for choice

    /**
     * Static method  for asking a choice input: y/n.
     *
     * @param message Show the prompt passed as argument: e.g. Do you wish to continue.
     *
     * @return boolean Returns the input from the user: e.g. y.
     */
    public static boolean askYOrN(String message) {

        while (true) {
            String input = ask(message + " (y/n)");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);

                switch (firstLetter) {
                    case 'y':
                        return true;
                    case 'n':
                        return false;
                    default:
                        throw new Exception();
                }
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
//                continue;
            }
        }
    }

    /**
     * Static method  for asking to choose a gender.
     *
     * @param message Show the prompt passed as argument: e.g. Choose a gender.
     *
     * @return Gender Returns the enum choice chosen by the user: e.g. Gender.MALE.
     */
    public static Gender askForGender(String message) {

        while (true) {
            String input = ask(message + " (m/f/o): ");
            char firstLetter = '0';
            try {
                firstLetter = input.toLowerCase().charAt(0);
            }
            catch (Exception e) {
                System.out.println(INVALID_MSG);
                continue;
            }
            switch (firstLetter) {
                case 'm':
                    return Gender.MALE;
                case 'f':
                    return Gender.FEMALE;
                case 'o':
                    return Gender.OTHER;
                default:
                    System.out.println(INVALID_MSG);
                    break;
            }
        }
    }

    /**
     * Static method for asking a choice.
     *
     * @param message Show the prompt passed as argument: e.g. Choose a date.
     * @param options A String array of possible choices.
     *
     * @return int The index of the item selected in the array.
     */
    public static int askForChoice(String message, String[] options) {

        System.out.println(message);
        return askForChoice(options);
    }

    /**
     * Static method invoked by askForChoice method with message.
     *
     * @param options String array of possible choices.
     *
     * @return int The index of the item selected in the array.
     */
    public static int askForChoice(String[] options) {

        while (true) {
            for (int i = 0; i < options.length; i++) {
                System.out.printf("%d. %s%n", i + 1, options[i]);
            }
            int chosenIdx = askForInt(String.format("Enter your choice (1-%d)", options.length)) - 1;
            if (chosenIdx < 0 || chosenIdx >= options.length) {
                System.out.println(INVALID_MSG);
                System.out.println("Please enter a choice in the valid range");
                System.out.println();
            }
            else {
                return chosenIdx;
            }
        }
    }


    //endregion

    //region ask for date-time

    /**
     * Static method for a date input with use of integers. It asks 3 questions: an int as year, an int as month and an int as day.
     *
     * @param message Show the prompt passed as argument: e.g. Enter a date.
     *
     * @return LocalDate Returns the LocalDate object created: e.g. 2021-8-12 (yyyy-MM-dd).
     */
    public static LocalDate askForFullDate(String message) {

        while (true) {
            System.out.println(message);
            int inputYear = askForInt("Year:");
            try {
                if (inputYear < 1900 || inputYear > LocalDate.now().getYear()) {
                    throw new IllegalDateException("Year must be >1900 and before this year");
                }
                int inputMonth = askForInt("Month (1-12):");
                if (inputMonth > 12 || inputMonth < 1) {
                    throw new IllegalDateException("Month must be between 1 and 12");
                }
                int daysInMonth = Month.of(inputMonth).length(Year.isLeap(inputYear));
                int inputDay = askForInt("Day (1-" + daysInMonth + "):");
                if (inputDay > daysInMonth || inputDay < 1) {
                    throw new IllegalDateException("Day must be between 1 and " + daysInMonth + " for " + Month.of(inputMonth));
                }
                return LocalDate.of(inputYear, inputMonth, inputDay);
            }
            catch (IllegalDateException ide) {
                System.out.println(INVALID_MSG);
                System.out.println(ide.getMessage());
            }
        }
    }

    /**
     * Static method for a date input in the format of yyyy/MM/dd.
     *
     * @param message Show the prompt passed as argument: e.g. Enter a date (2021/4/18).
     *
     * @return LocalDate Returns the formatted LocalDate object created: e.g. 2021-4-18.
     */
    public static LocalDate askForFormattedFullDate(String message) {

        while (true) {
            String dob = ask(message + " (e.g. 2021/8/12)");
            try {
                if (dob.length() > 0) {

                    String[] dateData = dob.split("/");
                    int year = Integer.parseInt(dateData[0]);
                    int month = Integer.parseInt(dateData[1]);
                    int day = Integer.parseInt(dateData[2]);

                    if (year < 1900 || year > LocalDate.now().getYear()) {
                        throw new NumberFormatException("Year must be >1900 and before this year");
                    }
                    if (month > 12 || month < 1) {
                        throw new NumberFormatException("Month must be between 1 and 12");
                    }
                    int daysInMonth = Month.of(month).length(Year.isLeap(year));
                    if (day > daysInMonth || day < 1) {
                        throw new NumberFormatException("Day must be between 1 and " + daysInMonth + " for " + Month.of(month));
                    }
                    return LocalDate.of(year, month, day);
                }
                else {
                    throw new IllegalDateException(INVALID_MSG);
                }
            }
            catch (ArrayIndexOutOfBoundsException aiob) {
                System.out.println("Date is not correct (e.g. 2021/8/15)");
            }
            catch (NumberFormatException nfe) {
                System.out.println("Date is not correct (e.g. 2021/8/15)");
            }
            catch (IllegalDateException ide) {
                System.out.println(ide.getMessage());
            }
        }
    }

    /**
     * Static method for time input in the format of HH:MM:SS.
     *
     * @param message Show the prompt passed as argument: e.g. Enter a start time (14:30:00).
     *
     * @return LocalTime Returns the formatted LocalTime object created: e.g. 14:30:00.
     */
    public static LocalTime askForTime(String message) {

        while (true) {
            String startTime = ask(message + " (14:30:00)");
            try {
                if (startTime.length() > 0) {

                    String[] dateData = startTime.split(":");
                    int hour = Integer.parseInt(dateData[0].trim());
                    int minute = Integer.parseInt(dateData[1].trim());
                    int second = Integer.parseInt(dateData[2].trim());


                    if (hour < 0 || hour > 23) {
                        throw new NumberFormatException("Hour must be between 0 and 23");
                    }
                    if (minute > 59 || minute < 0) {
                        throw new NumberFormatException("Minutes must be between 0 and 59");
                    }
                    if (second > 59 || second < 0) {
                        throw new NumberFormatException("Second must be between 0 and 59");
                    }
                    return LocalTime.of(hour, minute, second);

                }
                else {
                    throw new IllegalTimeException("Time format is incorrect: e.g. (14:30:00)");
                }

            }
            catch (ArrayIndexOutOfBoundsException aiobe) {
                System.out.println("Your date entry is not in the correct format (e.g. 14:30:00)");
            }
            catch (IllegalTimeException ite) {
                System.out.println(ite.getMessage());
            }
            catch (NumberFormatException nfe) {

                System.out.println(nfe.getMessage());
            }
        }
    }

    /**
     * Static method for asking for duration in minutes.
     *
     * @param message Show the prompt passed as argument: e.g. Enter the duration of the act (90).
     *
     * @return Duration Returns the Duration object created: e.g. 90.
     */
    public static Duration askForDuration(String message) {

        while (true) {
            String strDuration = ask(message + " (e.g. 90)");
            try {
                if (strDuration.length() > 0) {

                    int minutes = Integer.parseInt(strDuration.trim());

                    if (minutes < 0) {
                        throw new IllegalDateException("Minutes must be more 0 ");
                    }
                    return Duration.ofMinutes(minutes);
                }
                else {
                    throw new IllegalDateException("Time format is incorrect");
                }
            }
            catch (IllegalDateException ide) {
                System.out.println(INVALID_MSG);
                System.out.println(ide.getMessage());
            }
        }
    }

    //endregion

    //region ask for email

    /**
     * Static method for checking email address input, validated by a regular expression (user@domain.com).
     * The domain name must include at least one dot, and the part of the domain name after the
     * last dot can only consist of letters with at least 2 letters and a maximum of 6 letters.
     * No leading, trailing or consecutive dots allowed in the emails.
     * In the username, no single quotes or back ticks allowed.
     *
     * @param message Show the prompt passed as argument: e.g. Enter email (90).
     *
     * @return String Returns a String object containing a correct formatted email address: e.g. 90.
     */
    public static String askForEmail(String message) {

        while (true) {
            try {
                String input = ask(message);
//                final String EMAIL_PATTERN = "^[A-Za-z0-9+_.-]+@(.+)$";
                //no single quotes or back ticks allowed
                final String EMAIL_PATTERN = "^[\\w!#$%&*+/=?{|}~^-]+(?:\\.[\\w!#$%&*+/=?{|}~^-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$";
                if (input.matches(EMAIL_PATTERN)) {
                    return input;
                }
                else {
                    throw new IllegalEmailException("Email must be of pattern: user@domain.com");
                }
            }
            catch (IllegalEmailException iee) {
//                System.out.println(INVALID_MSG);
                System.out.println(iee.getMessage());
            }
        }
    }
    //endregion
}
