package org.tuxjitl.keyboardutility.menudecoration;

/**
 * This is an abstract UTILITY class for the menu decoration in a console application
 *
 * @author JCL
 * @version 1.0
 */
public abstract class MenuUtility {

    private static final int FULL_WIDTH = 32;

    //region Draw Lines

    /**
     * Static method for drawing a line containing a number of *.
     *
     * @return String A string containing a number of *
     */
    public static String thickLine(){
        return generateCharNTimes('*', FULL_WIDTH);
    }

    /**
     * Static method  for drawing a line containing a number of -.
     *
     * @return String A string containing a number of -
     */
    public static String thinLine(){
        return generateCharNTimes('-', FULL_WIDTH);
    }

    /**
     * Static method  for drawing a line containing a number of =.
     *
     * @return String A string containing a number of =.
     */
    public static String doubleThinLine(){
        return generateCharNTimes('=', FULL_WIDTH);
    }
    //endregion

    //region Helper methods


    /**
     * Static method  for generating lines of a certain length containing a particular character.
     *
     * @param c Char Is the character that you use to construct a line you want to display.
     * @param amount int The number of times you want to display the character when drawing a line.
     *
     * @return String The string containing the amount times the character.
     */
    private static String generateCharNTimes(char c, int amount) {
        StringBuilder text = new StringBuilder();
        for (int i = 0; i < amount; i++) {
            text.append(c);
        }
        return text.toString();
    }
    //endregion

    //region Text formatting

    /**
     * Static method  for centering text in a particular context (in a menu, line of text, ...).
     *
     * @param text The text you want to center.
     *
     * @return String Returns a String that is centered in the context requested.
     */
    public static String center(String text) {
        return String.format("%" + (FULL_WIDTH/2 + text.length()/2) + "s", text);
    }
    //endregion
}
